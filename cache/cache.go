package cache
import (
	"time"
	"errors"
)


type  Map interface{
	Get(string) (interface{},error)
	Set(string,interface{},int)
	Del(string)
	Clear()
}

type cacheSetFunc func()([]byte,error)

var (
	ErrNeedSet = errors.New("need set values")
	ErrTryAgain = errors.New("need try again")
)

const (
	GET int = iota
	SET int = iota
	DEL int = iota
	CLR int = iota
)

type cacheMap struct {
	mgr Map
	c chan *cmResult
	s map[string]error
}

type cmResult struct {
	k string
	method int
	c chan interface{}
	e chan error
	expires int
}


func wrap(m Map) *cacheMap{
	c := make(chan *cmResult,500)
	s := make(map[string]error)
	cm := &cacheMap{m,c,s}
	go func(){
		for cv := range(c){
			switch (cv.method){
			case GET:
				v,e := m.Get(cv.k)
				if e == nil {
					cv.c <- v
					cv.e <- nil
				}else{
					sv,sok := s[cv.k]
					if !sok {
						cv.e <- ErrNeedSet
						s[cv.k] = ErrTryAgain
					}else{
						cv.e <- sv
					}
				}
			case SET:
				delete(s,cv.k)
				item := <-cv.c
				m.Set(cv.k,item,cv.expires)
				cv.e <- nil
				if cv.expires > 0 {
					go func(k string,timeout int){
						<-time.After(time.Duration(int64(time.Second) * int64(timeout)))
						m.Del(k)
					}(cv.k,cv.expires)
				}
			case DEL:
				delete(s,cv.k)
				m.Del(cv.k)
			case CLR:
				m.Clear()
				s = make(map[string]error)
			}
		}
	}()
	return cm
}

func (c *cacheMap)MustGet(key string) (interface{},error){
	v,e := c.Get(key)
	if e == nil {
		return v,e
	}
	r := &cmResult{key,GET,make(chan interface{},1),make(chan error,1),0}
	c.c <-r
	e = <-r.e
	if e == nil {
		return <-r.c,nil
	}
	return nil,e

}

func (c *cacheMap)Get(key string) (interface{},error){
	return c.mgr.Get(key)
}

func (c *cacheMap)Set(key string,value interface{},expires int) {
	r := &cmResult{key,SET,make(chan interface{},1),make(chan error,1),expires}
	r.c <- value
	c.c <- r
	<-r.e
}

func (c *cacheMap)Del(key string){
	r := &cmResult{key,DEL,nil,nil,0}
	c.c <- r
}


func (c *cacheMap)Clear(){
	r := &cmResult{"",CLR,nil,nil,0}
	c.c <- r
}
