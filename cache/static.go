package cache
import (
	"path"
	"os"
	"net/http"
	"io/ioutil"
	"time"
)

var base string = ""

func Statify(path string){
	base = path
	os.RemoveAll(path)
	os.MkdirAll(path,os.ModeDir | 0755)
}

func cw(key string,f func()([]byte,error),w http.ResponseWriter,r *http.Request,timeout int,m *cacheMap){
	v,e := m.MustGet(key)
	fail := 0
	for e == ErrTryAgain && fail < 10 {
		fail++
		time.Sleep(10e6)
		v,e = m.MustGet(key)
	}
	if e == nil {
		w.Write(v.([]byte))
		return
	}
	fb,fe := f()
	if fe != nil {
		http.NotFound(w,r)
		if e == ErrNeedSet{
			m.Del(key)
		}
		return
	}else{
		if e == ErrNeedSet{
			m.Set(key,fb,timeout)
		}
		_,e := w.Write(fb)
		printError(e)
	}
	return
}

func Lcw(key string,f cacheSetFunc,w http.ResponseWriter,r *http.Request,timeout int){
	cw(key,f,w,r,timeout,L)
}

//func Mcw(key string,f cacheSetFunc,w http.ResponseWriter,r *http.Request,timeout int){
//	cw(key,f,w,r,timeout,M )
//}


func Ncw(key string,f cacheSetFunc,w http.ResponseWriter,r *http.Request,timeout int){
	fb,fe := f()
	if fe == nil {
		w.Write(fb)
		return
	}
	http.NotFound(w,r)
}

func  S(key string,f cacheSetFunc,w http.ResponseWriter,r *http.Request,timeout int){
	if base == "" {
		Lcw(key,f,w,r,timeout);
		return
	}
	fb,fe := f()
	if fe != nil {
		http.NotFound(w,r)
		return
	}
	w.Write(fb);
	p := path.Clean(base + "/" + key + ".html")
	pb := path.Clean(base + "/" + key + ".tmp")
	e := ioutil.WriteFile(pb,fb,0644)
	if e != nil {
		os.MkdirAll(path.Dir(pb), os.ModeDir | 0755)
		e = ioutil.WriteFile(pb,fb,0644)
		if e != nil {
			printError(e)
		}
	}

	if e == nil {
		os.Rename(pb,p)
		go func(){
			<-time.After(time.Duration(int64(time.Second) * int64(timeout)))
			os.Remove(p)
		}()
	}
}
