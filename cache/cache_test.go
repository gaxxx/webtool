package cache


import (
	"testing"
	"io/ioutil"
	"sync"
)


var toset []byte
var testMap map[string]interface{} = make(map[string]interface{})
var lck *sync.RWMutex = new (sync.RWMutex)

//func  BenchmarkMem(t *testing.B){
//	hit := 0
//	for i:=0;i<t.N;i++{
//		_,e :=  M.MustGet("key")
//		if e == nil {
//			hit++
//			continue
//		}
//		if e == ErrNeedSet {
//			M.Set("key",toset,10)
//		}
//	}
//	printf("mem hit %d/%d\n",hit,t.N)
//}



func  BenchmarkLocal(t *testing.B){
	hit := 0
	for i:=0;i<t.N;i++{
		_,e :=  L.MustGet("key")
		if e == nil {
			hit++
			continue
		}
		if e == ErrNeedSet {
			L.Set("key",toset,10)
		}
	}
	L.Del("key")
	printf("local hit %d/ %d\n",hit,t.N)
}

func  BenchmarkNative(t *testing.B){
	for i:=0;i<t.N;i++{
		lck.RLock()
		_,ok := testMap["key"]
		lck.RUnlock()
		if !ok{
			lck.Lock()
			if _,ok := testMap["key"];!ok {
				testMap["key"] = toset
			}
			lck.Unlock()
		}
	}
}

func TestLocal(t *testing.T){
	_,e := L.MustGet("key")
	if e == ErrNeedSet {
		L.Set("key",toset,10)
		_,e := L.MustGet("key")
		if e != nil {
			t.Fatal("error")
		}
	}else{
		t.Fatal("error1")
	}
}


func init(){
	b,_ := ioutil.ReadFile("testdata/test")
	toset = b
}
