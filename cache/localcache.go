package cache
import (
	"sync"
)




type lCache struct{
	l *sync.RWMutex
	m map[string]interface{}
}

func (c lCache)Get(key string) (interface{},error){
	c.l.RLock()
	v,ok := c.m[key]
	c.l.RUnlock()
	if !ok {
		return nil,ErrTryAgain
	}
	return v,nil
}

func (c lCache)Set(key string,value interface{},timeout int){
	c.l.Lock()
	c.m[key] = value
	c.l.Unlock()
}

func (c lCache)Del(key string){
	c.l.Lock()
	delete(c.m,key)
	c.l.Unlock()
}


func (c lCache)Clear(){
	c.l.Lock()
	c.m = make(map[string]interface{})
	c.l.Unlock()
}


func Lcm() *cacheMap {
	return wrap(lCache{new(sync.RWMutex),make(map[string]interface{})})
}

var L *cacheMap
func init(){
	L = Lcm()
}
