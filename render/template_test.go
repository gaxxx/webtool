package render


import (
	"testing"
	"bytes"
	"os/exec"
	"bufio"
)


func Test_Template(t *testing.T){
	Init("./tmpls",nil)
	printf("test  template\n")
	tmpl := Template("nest/sub.html")
	b := new(bytes.Buffer)
	e := tmpl.Execute(b,nil)
	printError(e)
	printf("test  template end\n")
}


func Test_Run(t *testing.T){
	printf("test  run\n")
	//c := exec.Command("inotifywait","-e","close_write", "-m", "--format",`"%f"`,"./")
	c := exec.Command("echo","test..............")
	stdout,_ := c.StdoutPipe()
	c.Start()
	for {
		outbr := bufio.NewReader(stdout)
		line,_,err := outbr.ReadLine()
		if err != nil {
			printf("%v\n",err)
			break
		}else{
			printf("out : %s\n",line)
		}
	}
	printf("test  run end\n")
}

