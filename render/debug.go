package render
import (
	"log"
)

func printf(format string,i ...interface{}){
	log.Printf(format,i...)
}


func printError(e error){
	if e != nil {
		log.Printf("%s",e)
	}
}
