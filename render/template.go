package render

import (
	"text/template"
	"path/filepath"
	"os"
	"io/ioutil"
//	"os/exec"
//	"bufio"
//	"time"
	"fmt"
	"reflect"
	"github.com/howeyc/fsnotify"
)

const DEBUG int = 1
const TFIX string = ".html"
func Template(name string)  *template.Template{
	if gtempStore == nil{
		panic("init temp dir first")
	}
	select {
		case  <-gtempStore.C:
			gtempStore.Load()
		default:
			break
	}
	return template.Must(gtempStore.Templates.Lookup(name),nil)
}



type tempStore struct {
	Templates *template.Template
	dir string
	C chan bool
	funcs template.FuncMap
}

var gtempStore *tempStore
var defaultFuncMap template.FuncMap = template.FuncMap {
			"numIndex" : numIndex,
			"charIndex" : charIndex,
			"same" : same,
			"array": array,
}

func (t *tempStore)Load(){
	defer func(){
		e := recover()
		if e != nil {
			switch e.(type) {
			case error:
				printf("Load %v\n",e)
			default:
				printf("default error\n")
				panic(e)
			}
		}
	}()

	tmpls := template.New("").Funcs(t.funcs)
	e := filepath.Walk(t.dir,func(path string,info os.FileInfo,err error) error{
		if info.IsDir() {
			return nil
		}
		if filepath.Ext(path) != TFIX {
			return nil
		}
		b,err := ioutil.ReadFile(path)
		if err != nil {
			printError(err)
			return nil
		}
		s := string(b)
		if t.dir != "." {
			path = path[len(t.dir)+1:]
		}
		tmpl := tmpls.New(path)
		_,err = tmpl.Parse(s)
		if err != nil {
			printError(err)
			return nil
		}
		return nil
	})
	tmpls = template.Must(tmpls,e)
	t.Templates = tmpls
}

func newTempStore(dir string,funcs template.FuncMap) *tempStore {
	t := &tempStore{nil,filepath.Clean(dir),make(chan bool,1),funcs}
	t.Load()
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		printf("%s\n",err)
	}

	go func(){
		for {
			select {
			case ev := <-watcher.Event :
				if ev.IsModify() {
					line := ev.Name
					if filepath.Ext(line) == TFIX {
						gtempStore.C <- true
					}
				}
			case err := <-watcher.Error :
				printf("error %s\n",err)
			}
		}
	}()
	watcher.Watch(dir)
	return t
}

func Init(dir string,funcs template.FuncMap){
	if funcs == nil {
		funcs = defaultFuncMap
	}
	for k,v := range(defaultFuncMap) {
		if funcs[k] == nil {
			funcs[k] = v
		}
	}
	gtempStore = newTempStore(dir,funcs)
}

func numIndex(i int )string {
	return fmt.Sprintf("%d",i+1)
}

func charIndex(i int) string {
	return "ABCDEFGHIJKLM"[i:i+1]
}

func same(a,b interface{}) bool {
	return reflect.DeepEqual(a,b)
}

func array(a int)[]int {
	return make([]int,a)
}
