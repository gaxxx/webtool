package render
import ( //"reflect"
"net/http"
"bytes"
)


type C map[string]interface{}
func (c C)Add(kv ...C){
	for _,v := range(kv){
		for mk,mv := range(v){
			c[mk] = mv
		}
	}
}


func (c C)m()map[string]interface{}{
	return map[string]interface{}(c)
}

func RenderTemplate(name string,ctx... C) ([]byte,error){
	c := C(make(map[string]interface{}))
	for _,v := range(ctx){
		c.Add(v)
	}
	t := Template(name)
	w := new(bytes.Buffer)
	e := t.Execute(w,c.m())
	printError(e)
	return w.Bytes(),e
}


func Render(name string,w http.ResponseWriter,ctx... C) error {
	b,e := RenderTemplate(name,ctx...)
	if e != nil {
		return e;
	}
	w.Write(b)
	return nil
}

